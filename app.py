from flask import Flask, render_template, request, redirect, url_for
from flask.blueprints import Blueprint
from models.pets import db, Species
from resources.api.pets import pets_api
from resources.api.species import species_api

def index():
    return render_template('index.html')


def create_tables():
    db.create_all()

def load_some_data():
    if Species.query.count() == 0:
        db.session.add(Species(name='Cat'))
        db.session.add(Species(name='Dog'))
    db.session.commit()

def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_ECHO'] = True
    app.config['DEBUG'] = True

    db.init_app(app)
    with app.app_context():
        create_tables()
        load_some_data()
    
    app.add_url_rule('/', view_func=index)

    api = Blueprint('api', __name__, url_prefix='/api')

    # Registramos los blueprints de la api
    api.register_blueprint(pets_api)
    api.register_blueprint(species_api)

    # Registramos la api REST en la app
    app.register_blueprint(api)
    return app

app = create_app()


