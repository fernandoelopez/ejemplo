from flask import Blueprint, request, jsonify
from models.pets import Species

species_api = Blueprint('species_api', __name__)

@species_api.get('/species')
def get_species():
    """
    Get all species
    """
    species = Species.query.all()
    return jsonify([{"id": s.id, "name": s.name} for s in species])
