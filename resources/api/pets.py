from flask import Blueprint, jsonify, request
from models.pets import Pet, db
from datetime import date, datetime

pets_api = Blueprint("pets_api", __name__)


@pets_api.get("/pets")
def get_pets():
    pets = Pet.query.all()
    return jsonify(
        [
            {"name": pet.name, "birthdate": pet.birthdate, "species": pet.species.name if pet.species else "Unknown"}
            for pet in pets
        ]
    )


@pets_api.post("/pets")
def create_pet():
    data = request.get_json()
    try:
        birthdate = datetime.fromisoformat(str(data["birthdate"]))
    except ValueError:
        birthdate = None
    pet = Pet(name=data["name"], birthdate=birthdate, species_id=data["species_id"])
    db.session.add(pet)
    db.session.commit()
    return "", 201
