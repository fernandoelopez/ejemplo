Installation
------------

```
git clone https://gitlab.com/fernandoelopez/ejemplo.git
cd ejemplo
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
```

Running
-------

```
FLASK_ENV=development FLASK_DEBUG=1 FLASK_APP=app:app flask run
```

Routes
------

* / is handled with templates
* /api/pets and /api/species are a RESTFUL JSON API
* /static/frontend/create_pet.html is a small VUE application

Limitations
-----------

vue-router is not used

URL for VUE application is ugly