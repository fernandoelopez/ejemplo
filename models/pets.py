from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Species(db.Model):
    __tablename__ = "species"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)


class Pet(db.Model):
    __tablename__ = "pets"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    birthdate = db.Column(db.Date)
    species_id = db.Column(db.Integer, db.ForeignKey("species.id"))
    species = db.relationship("Species")
